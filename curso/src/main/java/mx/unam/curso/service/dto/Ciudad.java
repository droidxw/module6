package mx.unam.curso.service.dto;

import java.util.List;

public class Ciudad {
	
	
	private Integer id;	
	private String nombre;
	private List<Temperatura> temperaturas;

	
    public List<Temperatura> getTemperaturas() {
		return temperaturas;
	}

	public void setTemperaturas(List<Temperatura> temperaturas) {
		this.temperaturas = temperaturas;
	}

	public Ciudad(Integer id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
