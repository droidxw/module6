package mx.unam.curso.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service; 

import mx.unam.curso.controller.CiudadController;
import mx.unam.curso.repository.CiudadRepository;
import mx.unam.curso.repository.TemperaturaRepository;
import mx.unam.curso.repository.dto.CiudadDTO;
import mx.unam.curso.repository.dto.TemperaturaDTO;
import mx.unam.curso.service.dto.Ciudad;
import mx.unam.curso.service.dto.Temperatura;

public interface TemperaturaService {
	
	public Temperatura crearTemperatura(Temperatura temperatura);

}
