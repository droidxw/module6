package mx.unam.curso.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import mx.unam.curso.controller.CiudadController;
import mx.unam.curso.repository.TemperaturaRepository;
import mx.unam.curso.repository.dto.TemperaturaDTO;
import mx.unam.curso.service.dto.Temperatura;
@Service
@ConditionalOnProperty(prefix="temp2", name="enabled", havingValue="false")
public class TemperaturaService1 implements TemperaturaService {
	
	
	private static Logger log=LoggerFactory.getLogger(CiudadController.class);
	
	@Autowired	
	private TemperaturaRepository temperaturaRepository;

	
	public Temperatura crearTemperatura(Temperatura temperatura) {
		TemperaturaDTO dto=new TemperaturaDTO (temperatura.getCiudadId(),temperatura.getValor(),
				temperatura.getFechaHora()); 
		dto=temperaturaRepository.crearTemperatura(dto);		
		
		return new Temperatura(dto.getId(),dto.getCiudadId(),dto.getValor(),dto.getFechaHora());
	}



}
