package mx.unam.curso.controller.dto.CiudadResponse;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CiudadRequest {
	
//	@NotNull(message="Error en el campo nombre, no debe ser null")
//	@NotNull(message="{nombre.error}")
	@NotBlank(message="{nombre.error}")
	@Size(max=50, message="{nombre.error.tamano}")
	
//	@NotBlank(message="No debe ser un caracter en blanco")
//	@Size(max=50, message="Nombre no puede ser mayor a 50")
//	@NotEmpty(message="No debe ser un campo vacio")
	private String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}
