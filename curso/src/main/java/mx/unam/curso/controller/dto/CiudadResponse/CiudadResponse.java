package mx.unam.curso.controller.dto.CiudadResponse;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import mx.unam.curso.service.dto.Temperatura;

public class CiudadResponse {
	
	
	private Integer id;
	@JsonProperty("nombre")
	private String name;
	@JsonInclude(Include.NON_NULL)
	private List<TemperaturaResponse> temperaturas;
	
	public List<TemperaturaResponse> getTemperaturas() {
		return temperaturas;
	}

	public void setTemperaturas(List<TemperaturaResponse> temperaturas) {
		this.temperaturas = temperaturas;
	}

	public CiudadResponse(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
