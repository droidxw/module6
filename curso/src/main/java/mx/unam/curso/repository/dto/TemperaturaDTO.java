package mx.unam.curso.repository.dto;

import java.time.LocalDateTime;

public class TemperaturaDTO {
	
	private Integer id;
	private Integer ciudadId;
	private Integer valor;
	private LocalDateTime fechaHora;
	
public TemperaturaDTO(Integer id, Integer ciudadid, Integer valor, LocalDateTime fechaHora) {
		super();
		this.id = id;
		this.ciudadId = ciudadid;
		this.valor = valor;
		this.fechaHora = fechaHora;
	}


public TemperaturaDTO( Integer ciudadid, Integer valor, LocalDateTime fechaHora) {
	super();
	
	this.ciudadId = ciudadid;
	this.valor = valor;
	this.fechaHora = fechaHora;
}



public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}
public Integer getCiudadId() {
	return ciudadId;
}
public void setCiudadId(Integer ciudadid) {
	this.ciudadId = ciudadid;
}
public Integer getValor() {
	return valor;
}
public void setValor(Integer valor) {
	this.valor = valor;
}
public LocalDateTime getFechaHora() {
	return fechaHora;
}
public void setFechaHora(LocalDateTime fechaHora) {
	this.fechaHora = fechaHora;
}

	

}
