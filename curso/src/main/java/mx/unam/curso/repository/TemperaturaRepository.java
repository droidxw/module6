package mx.unam.curso.repository;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;


import mx.unam.curso.repository.dto.TemperaturaDTO;

@Repository
public class TemperaturaRepository {
	private static final Logger log=LoggerFactory.getLogger(TemperaturaRepository.class);
	
	private static String SELECT_TEMPERATURAS="select *from temperatura where ciudad_id=?"; 
	private static String INSERT_TEMPERATURA="insert into temperatura(ciudad_id, valor, fecha_hora)  values (?,?,?)";
	private static String DELETE_TEMPERATURAS="delete from temperatura where ciudad_id=?";
	@Autowired	
	private JdbcTemplate jdbcTemplate;
	
	public List<TemperaturaDTO>obtenerTemperaturas(Integer ciudadId){
		
		return jdbcTemplate.query(SELECT_TEMPERATURAS,
				(rs, rowNum)-> new TemperaturaDTO(
						rs.getInt("id"), 
						rs.getInt("ciudad_id"),
						rs.getInt("valor"),
						rs.getTimestamp("fecha_hora").toLocalDateTime()),
				
				ciudadId);
		
	}
	
	
	public TemperaturaDTO crearTemperatura(TemperaturaDTO temperatura){
		KeyHolder keyHolder=new GeneratedKeyHolder ();
		jdbcTemplate.update(conn->{
			PreparedStatement ps=conn.prepareStatement(INSERT_TEMPERATURA, new String[] {"id"} );
			ps.setInt(1,temperatura.getCiudadId());
			ps.setInt(2,temperatura.getValor());
			ps.setTimestamp(3,Timestamp.valueOf(temperatura.getFechaHora()));			
			return ps;
		}, keyHolder);
		temperatura.setId(keyHolder.getKey().intValue());
		return 	temperatura;
		
	}
	
	public boolean borrarTemperaturaPorCiudadId(Integer ciudadId){
		int res= jdbcTemplate.update(DELETE_TEMPERATURAS,ciudadId);
		return res>=1;
		
		
	}
	

}
