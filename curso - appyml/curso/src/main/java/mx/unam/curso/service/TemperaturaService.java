package mx.unam.curso.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service; 

import mx.unam.curso.controller.CiudadController;
import mx.unam.curso.repository.CiudadRepository;
import mx.unam.curso.repository.TemperaturaRepository;
import mx.unam.curso.repository.dto.CiudadDTO;
import mx.unam.curso.repository.dto.TemperaturaDTO;
import mx.unam.curso.service.dto.Ciudad;
import mx.unam.curso.service.dto.Temperatura;
@Service
public class TemperaturaService {
	
	private static Logger log=LoggerFactory.getLogger(CiudadController.class);
	
	@Autowired	
	private TemperaturaRepository temperaturaRepository;

	
	public Temperatura crearTemperatura(Temperatura temperatura) {
		TemperaturaDTO dto=new TemperaturaDTO (temperatura.getCiudadId(),temperatura.getValor(),
				temperatura.getFechaHora()); 
		dto=temperaturaRepository.crearTemperatura(dto);		
		
		return new Temperatura(dto.getId(),dto.getCiudadId(),dto.getValor(),dto.getFechaHora());
	}


}
