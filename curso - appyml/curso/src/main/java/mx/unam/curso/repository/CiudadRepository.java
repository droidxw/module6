package mx.unam.curso.repository;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import excepciones.NoEncontradoException;
import mx.unam.curso.controller.CiudadController;
import mx.unam.curso.controller.dto.CiudadResponse.CiudadResponse;
import mx.unam.curso.repository.dto.CiudadDTO;

@Repository
@Transactional(propagation=Propagation.REQUIRES_NEW)
public class CiudadRepository {
	
	private static Logger log=LoggerFactory.getLogger(CiudadController.class);
	
	private static String SELECT_CIUDAD="select *from ciudad where id=?";
	private static String SELECT_CIUDADES="select *from ciudad ";
	private static String INSERT_CIUDAD="insert into ciudad(nombre) values(?)";
	private static String UPDATE_CIUDAD="update ciudad set nombre=? where id=?";
	private static String DELETE_CIUDAD="delete from ciudad where id=?";
	
	@Autowired	
	private JdbcTemplate jdbcTemplate;
	
	public CiudadDTO obtenerCiudad(Integer id) {
		try {
			
		return jdbcTemplate.queryForObject(SELECT_CIUDAD,
				(rs, rowNum)-> new CiudadDTO(rs.getInt("id"), rs.getString("nombre")),id);
		
		}catch(EmptyResultDataAccessException ex){
		
			throw new NoEncontradoException(id);
		}
	}	
	
	public List <CiudadDTO> obtenerCiudades() {
		return jdbcTemplate.query(SELECT_CIUDADES,
				(rs, rowNum)-> new CiudadDTO(rs.getInt("id"), rs.getString("nombre")));
		
	}
	
	public CiudadDTO crearCiudad(String nombre) {
		KeyHolder keyHolder=new GeneratedKeyHolder ();
		jdbcTemplate.update(connection->{
			PreparedStatement ps=connection.prepareStatement(INSERT_CIUDAD, new String[] {"id"} );
			ps.setString(1,nombre);
			return ps;
		}, keyHolder);
		return new CiudadDTO (keyHolder.getKey().intValue(),nombre);		
		
	}
	
	
	public CiudadDTO actualizarCiudad(CiudadDTO ciudad) {
		 int res =jdbcTemplate.update(UPDATE_CIUDAD, ciudad.getNombre(),  ciudad.getId());
		 return res==1? ciudad:null;
		 
	}
	
	
	public boolean borrarCiudad(Integer id) throws IOException{
//		 int res =jdbcTemplate.update(DELETE_CIUDAD, id);
//		 return res==1;
		throw new RuntimeException();
		 
	}	

}
