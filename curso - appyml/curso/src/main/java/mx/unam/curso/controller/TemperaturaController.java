package mx.unam.curso.controller;

import java.time.LocalDateTime;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import mx.unam.curso.controller.dto.CiudadResponse.TemperaturaRequest;
import mx.unam.curso.controller.dto.CiudadResponse.TemperaturaResponse;

import mx.unam.curso.service.TemperaturaService;
import mx.unam.curso.service.dto.Temperatura;
@RestController
@RequestMapping("/temperaturas")
public class TemperaturaController {
	private static Logger log=LoggerFactory.getLogger(TemperaturaController.class);
	@Autowired	
	private TemperaturaService temperaturaService;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public TemperaturaResponse creadTemperatura(@Valid @RequestBody TemperaturaRequest request) {
		
		Temperatura temperatura = new Temperatura(request.getCiudadId(), request.getValor(),LocalDateTime.now());
				
		temperatura=temperaturaService.crearTemperatura(temperatura);		
		return new TemperaturaResponse(temperatura.getId(), temperatura.getCiudadId(),temperatura.getValor(),temperatura.getFechaHora());
		
	}

}
