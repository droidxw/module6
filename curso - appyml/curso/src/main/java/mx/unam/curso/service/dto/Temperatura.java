package mx.unam.curso.service.dto;

import java.time.LocalDateTime;

public class Temperatura {
	
	private Integer id;
	private Integer ciudadId;
	private Integer valor;
	private LocalDateTime fechaHora;
	
public Temperatura(Integer ciudadId, Integer valor, LocalDateTime fechaHora) {
		super();
	
		this.ciudadId = ciudadId;
		this.valor = valor;
		this.fechaHora = fechaHora;
	}


public Temperatura(Integer id, Integer ciudadId, Integer valor, LocalDateTime fechaHora) {
	super();
	this.id=id;
	this.ciudadId = ciudadId;
	this.valor = valor;
	this.fechaHora = fechaHora;
}



public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}
public Integer getCiudadId() {
	return ciudadId;
}
public void setCiudadId(Integer ciudadid) {
	this.ciudadId = ciudadid;
}
public Integer getValor() {
	return valor;
}
public void setValor(Integer valor) {
	this.valor = valor;
}
public LocalDateTime getFechaHora() {
	return fechaHora;
}
public void setFechaHora(LocalDateTime fechaHora) {
	this.fechaHora = fechaHora;
}
	

}
