create table if not exists temperatura (
  id int not null auto_increment primary key,
 ciudad_id int not null,
 fecha_hora datetime not null,
 foreign key (ciudad_id)
     references ciudad(id)
);