package excepciones;

import java.util.Map;

public class NoEncontradoException extends RuntimeException {


	private String id;

	public NoEncontradoException(String id) {
		super();
		this.id = id;
	}
	
	
	public NoEncontradoException(Integer id) {
		super();
		this.id = String.valueOf(id);
	}

	
	public String getId() {
		return id;
	}
		
	
	
	
}
