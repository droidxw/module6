package mx.unam.curso.controller.dto.CiudadResponse;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TemperaturaRequest {
	
	
	@NotNull
	@Positive
	@JsonProperty("ciudad_id")
	private Integer ciudadId;
	@NotNull
	private Integer valor;
	
	public TemperaturaRequest() {
		super();
	}
	
	public TemperaturaRequest(Integer id, Integer ciudadId, Integer valor, LocalDateTime fechaHora) {
		super();	
		this.ciudadId = ciudadId;
		this.valor = valor;
	
	}

	public Integer getCiudadId() {
		return ciudadId;
	}
	public void setCiudadId(Integer ciudadId) {
		this.ciudadId = ciudadId;
	}
	public Integer getValor() {
		return valor;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	

	
	
	
	
}
