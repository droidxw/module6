package mx.unam.curso.controller.dto.CiudadResponse;

import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TemperaturaResponse {

	private Integer id;
	@JsonProperty("ciudad_id")
	@JsonInclude(Include.NON_NULL)
	private Integer ciudadId;
	private Integer valor;
	@JsonProperty("fecha_hora")
	@JsonFormat(pattern="dd-MM-yyyy hh:mm:ss")
	private LocalDateTime fechaHora;
	
	public TemperaturaResponse(@NotNull @Positive Integer ciudadId, @NotNull Integer valor) {
		super();
		this.ciudadId = ciudadId;
		this.valor = valor;
	}
	
	public TemperaturaResponse(Integer id, Integer ciudadId, Integer valor, LocalDateTime fechaHora) {
		super();
		this.id=id;
		this.ciudadId = ciudadId;
		this.fechaHora = fechaHora;
		this.valor = valor;
	}
	
	
	public LocalDateTime getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(LocalDateTime fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCiudadId() {
		return ciudadId;
	}
	public void setCiudadId(Integer ciudadId) {
		this.ciudadId = ciudadId;
	}
	public Integer getValor() {
		return valor;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	
	
}
