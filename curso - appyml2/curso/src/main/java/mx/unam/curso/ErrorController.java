package mx.unam.curso;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import excepciones.NoEncontradoException;
import mx.unam.curso.controller.CiudadController;
import mx.unam.curso.validation.ErrorValidacion;
@ControllerAdvice
public class ErrorController { 
	
	
	private static Logger log=LoggerFactory.getLogger(CiudadController.class);
	
	@Value("${error.descripcion}")
	private String errorDescription;
	
//	@ResponseStatus(HttpStatus.BAD_REQUEST) 
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity <ErrorValidacion> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
//		ErrorValidacion error=new ErrorValidacion("Eror de validacion");
		log.info("Controller Advice");
		ErrorValidacion error=new ErrorValidacion("errorDescripcion");		
		ex.getBindingResult().getFieldErrors()
		.forEach(e->error.addError(e.getField().toString(), e.getDefaultMessage()));
		return new ResponseEntity<> (error, HttpStatus.BAD_REQUEST);
		
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(NoEncontradoException.class)
	public void handleNotFoundException(NoEncontradoException ex) {
		log.error("Ciudad no encontrado:"+ ex.getId());

		
	}
	
	
	
}
