package mx.unam.curso.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import excepciones.NoEncontradoException;
import mx.unam.curso.controller.dto.CiudadResponse.CiudadRequest;
import mx.unam.curso.controller.dto.CiudadResponse.CiudadResponse;
import mx.unam.curso.controller.dto.CiudadResponse.TemperaturaResponse;
import mx.unam.curso.repository.CiudadRepository;
import mx.unam.curso.service.CiudadService;
import mx.unam.curso.service.dto.Ciudad;
import mx.unam.curso.validation.ErrorValidacion;


@RestController	
@RequestMapping("/ciudades")
public class CiudadController { 
	
	private static Logger log=LoggerFactory.getLogger(CiudadController.class);
	
	@Autowired	
	private CiudadService ciudadService;
	@Value("${error.descripcion}")
	private String errorDescripcion;
	
	@Value("${error.noexiste}")
	private String errorNo;
	
	
	
	
//	private List<CiudadResponse> ciudades;
//	
//	@GetMapping("/{id}")
//	@ResponseStatus(HttpStatus.OK)
//	public CiudadResponse obtenerCiudad(@PathVariable Integer id) {
//		return ciudades.get(id);	
//		
//	}
	
	
//	@GetMapping("/{id}")
//	@ResponseStatus(HttpStatus.OK)
//	public CiudadResponse obtenerCiudad(@PathVariable Integer id) {
//		Ciudad ciudad=ciudadService.obtenerCiudad(id);
//		return new CiudadResponse (ciudad.getId(),ciudad.getNombre());			
//	}
//	
	
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public CiudadResponse obtenerCiudad(@Valid @PathVariable Integer id) {
		log.info("ciudadController");
		Ciudad ciudad=ciudadService.obtenerCiudad(id);
//		return new CiudadResponse (ciudad.getId(),ciudad.getNombre());		
		CiudadResponse response =new CiudadResponse(ciudad.getId(),ciudad.getNombre());
		if(ciudad.getTemperaturas()!=null) {
			response.setTemperaturas(ciudad.getTemperaturas()
					.stream()
					.map(t->new TemperaturaResponse(t.getId(),t.getCiudadId(),t.getValor(),t.getFechaHora() ))
					.collect(Collectors.toList() ));
					
			
		}
		return response;
	}
	
//	@GetMapping
//	@ResponseStatus(HttpStatus.OK)
//	public List<CiudadResponse> obtenerCiudades(){
//		return ciudades;
//		
//	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List <CiudadResponse> obtenerCiudades() {
		return ciudadService.obtenerCiudades()
				.stream()
				.map(c->new CiudadResponse(c.getId(),c.getNombre()))
				.collect(Collectors.toList());		
	}
	
	
	
//	@PostMapping
//	@ResponseStatus(HttpStatus.CREATED)
//	public void creadCiudad(@RequestBody CiudadRequest request) {
//		
//		CiudadResponse ciudad = new CiudadResponse(ciudades.size(), request.getNombre());
//		ciudades.add(ciudad);
//	}
	
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public CiudadResponse creadCiudad(@Valid @RequestBody CiudadRequest request) {
		
		Ciudad ciudad = ciudadService.crearCiudad(request.getNombre());		
		return new CiudadResponse(ciudad.getId(), ciudad.getNombre());
		
	}
	
//	@PutMapping("/{id}")
//	@ResponseStatus(HttpStatus.OK)
//	public void actualizarCiudad(@PathVariable Integer id, @RequestBody CiudadRequest request) {
//		
//		CiudadResponse ciudad = ciudades.get(id);
//		ciudad.setName(request.getNombre());
//	}
	
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void actualizarCiudad( @PathVariable Integer id, @Valid @RequestBody CiudadRequest request) {
		
		ciudadService.actualizarCiudad(new Ciudad(id,request.getNombre()));
	}
	

	//throwable requiere de try&cath o de otra throwable las demas heredan de runtime
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void borrarCiudad(@PathVariable Integer id) throws IOException {
		ciudadService.borrarCiudad(id);
		
	}
	
	
//	@PostConstruct
//	public void inicializar() {
//		ciudades=new ArrayList<>();
//		ciudades.add(new CiudadResponse(0, "Toluca"));
//		ciudades.add(new CiudadResponse(1, "Pachuca"));
//		log.info("Ciudad controller inicializado");
//		
//		
//	}
	
//método inconcluso para devolver un 404
//	@ResponseStatus(HttpStatus.BAD_REQUEST)
//	@ExceptionHandler(MethodArgumentNotValidException.class)
//	public Map<String, String>handleMethodAtgumentNotValid(MethodArgumentNotValidException ex) {
//		Map<String, String> errors=new HashMap<>();
//		ex.getBindingResult().getFieldErrors().forEach(error->
//		errors.put(error.getField(), error.getDefaultMessage()));
//		return errors;
//		
//	}
	
	
	///***
//	@ResponseStatus(HttpStatus.BAD_REQUEST)
//	@ExceptionHandler(MethodArgumentNotValidException.class)
//	public ErrorValidacion handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
////		ErrorValidacion error=new ErrorValidacion("Eror de validacion");
//		ErrorValidacion error=new ErrorValidacion(errorDescripcion);		
//		ex.getBindingResult().getFieldErrors()
//		.forEach(e->error.addError(e.getField().toString(), e.getDefaultMessage()));
//		return error;
//		
//	}
	
	
//	@ResponseStatus(HttpStatus.NOT_FOUND)
//	@ExceptionHandler(MethodArgumentNotValidException.class)
//	public ErrorValidacion handleMethodArgumentNotFound(MethodArgumentNotValidException ex) {
////		ErrorValidacion error=new ErrorValidacion("Eror de validacion");
//		ErrorValidacion error=new ErrorValidacion(errorNo);		
//		ex.getBindingResult().getFieldErrors()
//		.forEach(e->error.addError(e.getField().toString(), e.getDefaultMessage()));
//		return error;
//		
//	}
	
	///***
//	@ResponseStatus(HttpStatus.NOT_FOUND)
//	@ExceptionHandler(NoEncontradoException.class)
//	public void handleNotFoundException(NoEncontradoException ex) {
//		log.error("Ciudad no encontrado:"+ ex.getId());
//
//		
//	}
	
	
	
}
