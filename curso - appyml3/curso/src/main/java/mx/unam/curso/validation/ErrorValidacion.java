package mx.unam.curso.validation;

import java.util.HashMap;
import java.util.Map;

public class ErrorValidacion {
	
	private String descripcion;
	private Map<String, String > errors;	
	
	public ErrorValidacion(String descripcion) {
		this.descripcion=descripcion;
		errors=new HashMap<>();
			}	
	
	public String getDescripcion() {
		return descripcion;
	}	
	
	public Map<String, String> getErrors() {
		return errors;
	}	
	
	public void addError (String campo, String mensaje) {
		 errors.put(campo, mensaje);
	}
		
}
