package mx.unam.curso.service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.unam.curso.controller.CiudadController;
import mx.unam.curso.repository.CiudadRepository;
import mx.unam.curso.repository.TemperaturaRepository;
import mx.unam.curso.repository.dto.CiudadDTO;
import mx.unam.curso.service.dto.Ciudad;
import mx.unam.curso.service.dto.Temperatura;

@Service
public class CiudadService {
	
	private static Logger log=LoggerFactory.getLogger(CiudadController.class);
	
	@Autowired	
	private CiudadRepository ciudadRepository;
	
	@Autowired	
	private TemperaturaRepository temperaturaRepository;
	
	public Ciudad obtenerCiudad(Integer id) {
		CiudadDTO dto=ciudadRepository.obtenerCiudad(id); 
//		return new Ciudad(dto.getId(),dto.getNombre());
		Ciudad ciudad=new Ciudad(dto.getId(),dto.getNombre());
		ciudad.setTemperaturas(temperaturaRepository.obtenerTemperaturas(dto.getId())
				.stream()
				.map(t-> new Temperatura(t.getId(),null, t.getValor(),t.getFechaHora() ))
				.collect(Collectors.toList() ));
		return ciudad;
	}
	
	
	public List <Ciudad> obtenerCiudades() {
		return ciudadRepository.obtenerCiudades()
				.stream()
				.map(d->new Ciudad(d.getId(),d.getNombre()))
				.collect(Collectors.toList());		
	}
	
	
	public Ciudad crearCiudad(String nombre) {
		CiudadDTO dto=ciudadRepository.crearCiudad(nombre); 
		return new Ciudad(dto.getId(),dto.getNombre());
	}

	
	
	public Ciudad actualizarCiudad(Ciudad ciudad) {
		CiudadDTO dto=ciudadRepository.actualizarCiudad(
				new CiudadDTO(ciudad.getId(),ciudad.getNombre())); 
				return dto==null? null:ciudad;
	}	
	
	//a nivel en campa de servicios operaciones  de ir a vatios repos o hacer varias operaciones
//	@Transactional(propagation=Propagation.REQUIRED)
	@Transactional
	public void borrarCiudad(Integer id) throws IOException{
		temperaturaRepository.borrarTemperaturaPorCiudadId(id);
		ciudadRepository.borrarCiudad(id);
			
	}
	
	
}
