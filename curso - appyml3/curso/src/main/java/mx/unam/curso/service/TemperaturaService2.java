package mx.unam.curso.service;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import mx.unam.curso.controller.CiudadController;
import mx.unam.curso.repository.TemperaturaRepository;
import mx.unam.curso.repository.dto.TemperaturaDTO;
import mx.unam.curso.service.dto.Temperatura;
//código en duro
@Service
@ConditionalOnProperty(prefix="temp2", name="enabled", havingValue="true")
public class TemperaturaService2 implements TemperaturaService {
	private static Logger log=LoggerFactory.getLogger(CiudadController.class);


	
	public Temperatura crearTemperatura(Temperatura temperatura) {
	
		return new Temperatura(1, temperatura.getCiudadId(),22, LocalDateTime.now());
	}


}
